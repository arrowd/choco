﻿$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$packageArgs = @{
    packageName    = $env:ChocolateyPackageName
    fileType       = 'msi'
    url            = 'https://downloads.iridiumbrowser.de/windows/2021.06.91/iridiumbrowser-2021.06.91.0-x86.msi'
    url64bit       = 'https://downloads.iridiumbrowser.de/windows/2021.06.91/iridiumbrowser-2021.06.91.0-x64.msi'
    checksum       = '788bf6dd7592ef6eeaa3216d34752567c78861c662a482b9141c713692db6f7b'
    checksum64     = 'b546f48255dc202d9689fc5d772d167415def0c354055a2c458792f766eaa86f'
    checksumType   = 'sha256'
    checksumType64 = 'sha256'
    silentArgs     = "/qn /norestart"
}

Install-ChocolateyPackage @packageArgs

Get-ChildItem $toolsDir\*.msi | ForEach-Object { Remove-Item $_ -ea 0; if (Test-Path $_) { Set-Content "$_.ignore" } }
