﻿$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$packageArgs = @{
  packageName    = $env:ChocolateyPackageName
  url            = 'http://pmetro.chpeks.com/download/pMetroSetup.exe'
  checksum       = '422795f59c6053cd548a61395c92d60b304e0a3cd17ea636234e24189556ffbd'
  checksumType   = 'sha256'
  silentArgs     = '/VERYSILENT /SUPPRESSMSGBOXES /NORESTART /SP-'
}

Install-ChocolateyPackage @packageArgs

Remove-Item $toolsDir\*.exe -ea 0
