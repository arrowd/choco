﻿$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$installArgs = '/S /D=' + $toolsDir

$packageArgs = @{
  packageName    = $env:ChocolateyPackageName
  url            = 'https://dist.torproject.org/torbrowser/11.0a7/torbrowser-install-11.0a7_en-US.exe'
  url64bit       = 'https://dist.torproject.org/torbrowser/11.0a7/torbrowser-install-win64-11.0a7_en-US.exe'
  checksum       = '8b2013669d88e3ae8fa9bc17a3495eaac9475f79a849354e826e5132811a860b'
  checksum64     = '5714bb90b886b56f8311c627253c9fc0c2b770c730320f678fc19e39e9600f55'
  checksumType   = 'sha256'
  checksumType64 = 'sha256'
  silentArgs     = $installArgs
}

Install-ChocolateyPackage @packageArgs

$files = Get-ChildItem $toolsDir -include *.exe -recurse
foreach ($file in $files) {
    New-Item "$file.ignore" -type file -force | Out-Null
}
