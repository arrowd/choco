import-module au

$releases = 'https://bintray.com/colinduquesnoy/MellowPlayer'

function global:au_GetLatest {
    $page = Invoke-WebRequest -Uri $releases -UseBasicParsing
    $version = ($page.links | ? href -match 'Stable' | select -Last 1 -expand href) -split '/' | select -Last 1
    $version_c = ($page.links | ? href -match 'Continuous' | select -Last 1 -expand href) -split '/' | select -Last 1

    if ([version]$version -gt [version]$version_c) {
        @{
            Version = $version
            URL32   = $releases + '/download_file?file_path=stable/' + $version + '/MellowPlayer_Setup.exe'
        }
    } else {
        @{
            Version = $version_c + '-continuous'
            URL32   = $releases + '/download_file?file_path=continuous/' + $version_c + '/MellowPlayer_Setup.exe'
        }
    }
}

function global:au_BeforeUpdate {
    Invoke-WebRequest -Uri 'https://gitlab.com/ColinDuquesnoy/MellowPlayer/raw/master/LICENSE' -OutFile "legal\LICENSE.txt"
    Get-RemoteFiles -Purge -NoSuffix
}

function global:au_SearchReplace {
   @{
    ".\tools\chocolateyInstall.ps1" = @{
        "(?i)(^\s*file\s*=\s*`"[$]toolsDir\\).*" = "`${1}$($Latest.FileName32)`""
        }
    ".\legal\VERIFICATION.txt" = @{
        "(?i)(x86:).*"        = "`${1} $($Latest.URL32)"
        "(?i)(checksum32:).*" = "`${1} $($Latest.Checksum32)"
        }
    }
}

update -ChecksumFor none
