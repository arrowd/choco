param([string[]] $Name, [string] $Root = $PSScriptRoot)

$Options = [ordered]@{
    RepeatOn = @( 'Choco pack failed with exit code 1'
                  'The operation has timed out' )
    Push   = $Env:au_Push -eq 'true'
    Report = @{ Path = "$PSScriptRoot\Update-AUPackages.md" }
    Snippet = @{
        Path     = "$PSScriptRoot\Update-AUPackages.md"
        Id       = $Env:snippet_id
        ApiToken = $Env:gitlab_api_token
        FileName = "StablePackages.md"
        }
    Git = @{
        User     = $Env:gitlab_user
        Password = $Env:gitlab_password
        Branch   = 'stable'
        }
    BeforeEach = {
        param($PackageName, $Options )

        $p = $Options.ForcedPackages | ? { $_ -match "^${PackageName}(?:\\(?<stream>[^:]+))?(?:\:(?<version>.+))?$" }
        if (!$p) { return }

        $global:au_Force         = $true
        $global:au_IncludeStream = $Matches['stream']
        $global:au_Version       = $Matches['version']
    }
}

#[System.Net.ServicePointManager]::SecurityProtocol = 3072 -bor 768 -bor [System.Net.SecurityProtocolType]::Tls -bor [System.Net.SecurityProtocolType]::Ssl3

$global:info = updateall -Name $Name -Options $Options

if ($global:info.error_count.total) { throw 'Errors during update' }
