﻿$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$packageArgs = @{
  packageName    = $env:ChocolateyPackageName
  url            = 'https://download.immunet.com/binaries/immunet/bin/ImmunetSetup.exe'
  checksum       = '7917a346a77ea33514f1608c0ed87d13935c00ca100059d893ddb4224263d7fe'
  checksumType   = 'sha256'
  silentArgs     = '/S'
}

Install-ChocolateyPackage @packageArgs

Get-ChildItem $toolsDir\*.exe | ForEach-Object { Remove-Item $_ -ea 0; if (Test-Path $_) { Set-Content "$_.ignore" } }
